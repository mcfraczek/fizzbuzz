package com.company;

import java.util.stream.IntStream;

//Write a program that prints the numbers from 1 to 100.
// But for multiples of three print “Fizz” instead of the number and for the multiples of five print “Buzz”.
// For numbers which are multiples of both three and five print “FizzBuzz”.
public class FizzBuzz {

    public static void main(String[] args) {
        IntStream.rangeClosed(1, 100)
                .mapToObj(number -> {
                    if (number % 3 == 0 && number % 5 == 0) {
                        return "FizzBuzz";
                    } else if (number % 3 == 0) {
                        return "Fizz";
                    } else if (number % 5 == 0) {
                        return "Buzz";
                    }
                    return number;
                })
                .forEach(System.out::println);
    }

}
